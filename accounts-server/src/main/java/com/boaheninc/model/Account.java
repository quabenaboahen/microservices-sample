package com.boaheninc.model;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {
	
	private int id;
	private String number;
	private String owner;
	private BigDecimal balance;
	
}
