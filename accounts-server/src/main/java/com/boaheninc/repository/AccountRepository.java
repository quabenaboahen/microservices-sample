package com.boaheninc.repository;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.boaheninc.model.Account;

@Repository
public class AccountRepository {
	
	private final List<Account> ACCOUNTS_LIST=Arrays.asList(
			new Account(1, "001", "Donytella Fortiny", new BigDecimal(25000.00)), 
			new Account(2, "002", "Deni Makafui", new BigDecimal(26000.00)),
			new Account(3, "003", "Quabena Boahen", new BigDecimal(85000.00)));
	
	
  public List<Account> populateAccounts(){
		return ACCOUNTS_LIST;
	}

}
