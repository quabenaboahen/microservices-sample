package com.boaheninc.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RefreshScope
public class IndexController {
	
	@Value("${welcomeMessage: Config server is not working.. please check !}")
	private String accountServiceWelcomeMessage;

	@ResponseBody
	@RequestMapping(method= RequestMethod.GET, value="/")
	public String index() {
		return accountServiceWelcomeMessage;
	}

}
