package com.boaheninc.web;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.boaheninc.exceptions.AccountNotFoundException;
import com.boaheninc.model.Account;
import com.boaheninc.service.AccountsService;
import io.swagger.annotations.ApiOperation;

/*
 * Copyright 2018 Boahen Fred
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@RestController
@Produces({MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
@Consumes({MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
public class AccountController {
	
	@Autowired
	private AccountsService accountService;

	@RequestMapping(method= RequestMethod.GET, value="/accounts")
	@ApiOperation(value="Get all accounts", notes="Returns the list of all accounts")
	public List<Account> allAccounts() {
		return accountService.getAllAccounts();
	}
	
	@RequestMapping(method= RequestMethod.GET, value="/accounts/{accountId}")
	@ApiOperation(value="Get account details by accountId", notes="Returns the account details when accountId is specified")
	public Account findAccountById(@PathVariable("accountId") Integer accountId) {
		if(accountService.findAccountById(accountId)==null) {
			throw new AccountNotFoundException(accountId);
		 }else{
		  return accountService.findAccountById(accountId);
		}
	}

}
