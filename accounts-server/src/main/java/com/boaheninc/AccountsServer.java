package com.boaheninc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;
import com.boaheninc.config.SwaggerConfig;

@SpringBootApplication
@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(SwaggerConfig.class)
public class AccountsServer {
	
	public static void main(String[] args) {
	   SpringApplication.run(AccountsServer.class, args);
	}

}
