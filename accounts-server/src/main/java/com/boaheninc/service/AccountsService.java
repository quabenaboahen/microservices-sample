package com.boaheninc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.boaheninc.model.Account;
import com.boaheninc.repository.AccountRepository;

@Service
public class AccountsService {
	
	protected String serviceUrl;
	
	@Autowired
	private AccountRepository accountRepository;
	
	public List<Account> getAllAccounts(){
		return accountRepository.populateAccounts();
	}
	
	public Account findAccountById(int accountId) {
		Account foundAccount=null;
		for(Account account : accountRepository.populateAccounts()) {
			if(account.getId()==accountId) {
				foundAccount=account;
			}
		}
	    return foundAccount;
	}

}
