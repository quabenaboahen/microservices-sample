package com.boaheninc.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;

import com.boaheninc.model.Account;

@Service
public class AccountService {
	
	@Autowired
	@LoadBalanced
	protected RestTemplate restTemplate;
	
	protected String ACCOUNTS_SERVICE_URL="http://ACCOUNTS-SERVICE-CLIENT/";
	
	private String serviceUrl;
	
	public AccountService() {
		this.serviceUrl=ACCOUNTS_SERVICE_URL;
	}
	
	@SuppressWarnings("unchecked")
	public List<Account> getAllAccounts() {
		return restTemplate.getForObject(serviceUrl + "accounts", List.class);
	}
	
	
	public Account findAccountById(@PathVariable("accountId") Integer accountId) {
		return restTemplate.getForObject(serviceUrl + "accounts/" + accountId, Account.class);
	}

}
