package com.boaheninc.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import com.boaheninc.service.AccountService;
import com.boaheninc.web.AccountsController;

@Configuration
@ComponentScan
public class WebServiceConfiguration {
	
	@LoadBalanced
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public AccountService accountsService() {
		return new AccountService();
	}
	
	@Bean
	public AccountsController accountsController() {
		return new AccountsController(accountsService());
	}

}
