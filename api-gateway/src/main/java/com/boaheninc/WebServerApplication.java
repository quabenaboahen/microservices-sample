package com.boaheninc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

import com.boaheninc.config.WebServiceConfiguration;

@SpringBootApplication
@EnableDiscoveryClient
@Import(WebServiceConfiguration.class)
public class WebServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebServerApplication.class, args);
	}
	
}
