package com.boaheninc.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class HomeController {
	
	@Value("${welcomeMessage: Config server for application not functioning.. Please check !}")
	private String welcomeMessage;
	
	@RequestMapping(method= RequestMethod.GET, value="/")
	public String home() {
		return this.welcomeMessage;
	}

}
