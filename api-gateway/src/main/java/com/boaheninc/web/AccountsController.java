package com.boaheninc.web;

import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.boaheninc.model.Account;
import com.boaheninc.service.AccountService;

@RestController
@RequestMapping("/api/v1")
public class AccountsController {
	
	protected AccountService webAccountService;
	
	public AccountsController(AccountService webAccountService) {
	    this.webAccountService=webAccountService;
	}
	
	@RequestMapping(method= RequestMethod.GET, value="/accounts", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Account> getAllAccounts() {
		return webAccountService.getAllAccounts();
	}
	
	@RequestMapping(method= RequestMethod.GET, value="/accounts/{accountId}", produces=MediaType.APPLICATION_JSON_VALUE)
	public Account findAccountById(@PathVariable("accountId") Integer accountId) {
		return webAccountService.findAccountById(accountId);
	}

}
