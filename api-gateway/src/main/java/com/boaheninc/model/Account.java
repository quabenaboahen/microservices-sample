package com.boaheninc.model;

import java.math.BigDecimal;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = Account.class)
public class Account {
	
	private int id;
	private String number;
	private String owner;
	private BigDecimal balance;
	
	public Account() {
		
	}
	
	public Account(int id, String number, String owner, BigDecimal balance) {
		this.id = id;
		this.number = number;
		this.owner = owner;
		this.balance = balance;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}	

}
