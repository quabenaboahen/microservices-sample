package com.boaheninc.filter;

import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

/**
@author Boahen Fred
*
*/
public class PreFilter extends ZuulFilter{
	
	private static Logger log = LoggerFactory.getLogger(PreFilter.class);

	@Override
	public Object run() {
		RequestContext context = RequestContext.getCurrentContext();
		HttpServletRequest request = context.getRequest();	
		try {
			log.info(String.format("%s request from %s to %s", request.getMethod(), InetAddress.getLocalHost().toString(), request.getRequestURL().toString()));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public String filterType() {	
		return "pre";
	}
	
}
